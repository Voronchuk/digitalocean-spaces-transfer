# DigitalOcean Spaces Transfer
If you need to transfer files from one DigitalOcean Space to another e.g. to move the Space to another account, then this PHP script should help you. 

**Instructions:**
1. Clone the script on a $15 Droplet with 3GB RAM in the same region of your source or target Space for faster transfers.
2. Run `composer install`
3. Create '[Spaces Access Keys](https://cloud.digitalocean.com/account/api/tokens)' for source and target Space.
4. Edit run-me.php and update the key, secret, bucket (space name) and region variables.
5. Run the `run-me.php` script and watch the progress live in the browser. You are free to stop and refresh the script at any time, it will continue the transfer without uploading existing files.

**What will be transferred?**
* All your files will be copied with existing folder structure
* Permissions (public / private)
* Mime ContentType
---
Runs on Spaces API v1 by SociallyDev ♥
https://github.com/SociallyDev/Spaces-API

Spaces API is using AWS SDK  to connect to DigitalOcean Spaces. This means you can use this script to transfer files between AWS S3 and DigitalOcean Spaces in any combination.

**Known issue:**
In case your Space does not have SSL enabled, Spaces API will throw an error. To fix that, after running `composer install` find the `vendor\sociallydev\spaces-api\aws\GuzzleHttp\Handler\CurlFactory.php` and update the code starting at line 323 with:

```
$conf[CURLOPT_SSL_VERIFYHOST] = false;
$conf[CURLOPT_SSL_VERIFYPEER] = false;
} else {
$conf[CURLOPT_SSL_VERIFYHOST] = false;
$conf[CURLOPT_SSL_VERIFYPEER] = false;
```
---
**We at Exevio had this issue moving Spaces to another account for our clients and since DigitalOcean does not support this feature yet, we decided to build our own solution, a simple script that could help others, not just us. Made with passion.* 

Feel free to **contribute** by improving this code and it's features ♥